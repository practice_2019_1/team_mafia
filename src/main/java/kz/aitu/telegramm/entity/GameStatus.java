package kz.aitu.telegramm.entity;

public enum GameStatus {

    REGISTRATION, DAY, NIGHT, ELECTION, ENDGAME
}
