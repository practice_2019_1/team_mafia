package kz.aitu.telegramm.entity;

import lombok.Data;

@Data
public class Player {

    Long id;
    String chatID;
    String fname;
    boolean isAlive;
    Role role; //CIVIL MAFIA
    int wins;
}
