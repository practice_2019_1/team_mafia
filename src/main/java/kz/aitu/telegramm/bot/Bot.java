package kz.aitu.telegramm.bot;

import kz.aitu.telegramm.entity.GameStatus;
import kz.aitu.telegramm.entity.Player;
import kz.aitu.telegramm.entity.Role;
import kz.aitu.telegramm.repository.DB;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.toIntExact;

public class Bot extends TelegramLongPollingBot {

    private String BOT_NAME;
    private String BOT_TOKEN;

    private int limitPlayer = 3;
    int countMafia = 2;
    private static GameStatus gameStatus = GameStatus.REGISTRATION;


    private static Map<String, Player> chats = new HashMap<String, Player>();

    public Bot(String BOT_NAME, String BOT_TOKEN) {
        this.BOT_NAME = BOT_NAME;
        this.BOT_TOKEN = BOT_TOKEN;
    }
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String chatID = update.getMessage().getChatId().toString();
            String message = update.getMessage().getText();
            String fname = update.getMessage().getChat().getFirstName();
            //String username = update.getMessage().getChat().getUserName();

            System.out.println(chatID + ": " + message);
            //sendMsg("-1001187454388", "MESSAGE-" + message);


            if (message.startsWith("/start")) {
                if (gameStatus == GameStatus.REGISTRATION) {
                    Player player = new Player();
                    player.setChatID(chatID);
                    player.setFname(fname);
                    player.setAlive(true);
                    player.setRole(Role.CIVIL);
                    chats.put(chatID, player);
                    DB.addPlayer(player);

                    if (chats.size() >= limitPlayer) {
                        startGame();
                    } else {
                        broadcastMsg(player.getFname() + " joined. \n waiting " + (limitPlayer - chats.size()) + " players.");
                    }
                }
            } else if (message.startsWith("/list")) {
                sendMsg(chatID, getListPlayers());
            } else if (message.startsWith("/rating")) {
                sendMsg(chatID, getPlayerList());
            } else {
                if(chats.get(chatID).isAlive()) {
                    if (gameStatus == GameStatus.DAY) broadcastMsg(message);
                    if (gameStatus == GameStatus.NIGHT && chats.get(chatID).getRole() == Role.MAFIA)
                        broadcastMsgRole(message, Role.MAFIA, chatID);
                }
            }
        } else if (update.hasCallbackQuery()) {
            // Set variables
            String call_data = update.getCallbackQuery().getData();
            long message_id = update.getCallbackQuery().getMessage().getMessageId();
            String chat_id = update.getCallbackQuery().getMessage().getChatId().toString();

            if (call_data.startsWith("kill_player_")) {
                String killedPlayerChatId = call_data.substring(12);

                if(gameStatus == GameStatus.NIGHT && chats.get(chat_id).getRole() == Role.MAFIA && chats.get(chat_id).isAlive()) {
                    EditMessageText new_message = new EditMessageText()
                            .setChatId(chat_id)
                            .setMessageId(toIntExact(message_id))
                            .setText("You killed: " + chats.get(killedPlayerChatId).getFname());
                    try {
                        execute(new_message);
                        broadcastMsg("Mafia killed: " + chats.get(killedPlayerChatId).getFname());

                        chats.get(killedPlayerChatId).setAlive(false);
                        setGameStatus(GameStatus.DAY);
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }


    public String getListPlayers() {
        StringBuffer sb = new StringBuffer();
        int count = 1;
        for (Player player: chats.values()) {
            if(player.isAlive()) {
                sb.append(count + ". " + player.getFname() + " " + "\n");
                count++;
            }
        }
        return sb.toString();
    }


    private SendMessage addKeyboard(SendMessage sendMessage) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        row.add("/start");
        row.add("/list");
        row.add("/rating");
        keyboard.add(row);
        row = new KeyboardRow();
        row.add("/help");
        row.add("/authors");
        row.add("/status");
        keyboard.add(row);
        keyboardMarkup.setKeyboard(keyboard);
        sendMessage.setReplyMarkup(keyboardMarkup);

        return sendMessage;
    }



    private void startGame() {
        broadcastMsg("GAME STARTED! \n You are in game!");
        broadcastMsg("Setting roles...");


        Random random = new Random();
        while(countMafia > 0) {
            for (Player player: chats.values()) {
                if(player.getRole() != Role.CIVIL) continue;
                int rNumber = random.nextInt(chats.size());
                if(rNumber < countMafia) {
                    player.setRole(Role.MAFIA);
                    countMafia--;
                }

                if(countMafia <= 0) break;
            }
        }

        for (Player player: chats.values()) {
            sendMsg(player.getChatID(), "Your role: " + player.getRole());
        }



        setGameStatus(GameStatus.NIGHT);
    }



    void setGameStatus(GameStatus status) {
        gameStatus = status;

        if(status == GameStatus.NIGHT) {
            broadcastMsg("GAME GOES TO NIGHT\n");
            for (Player player: chats.values()) {
                if(player.getRole() == Role.MAFIA) {
                    SendMessage sendMessage = new SendMessage();
                    sendMessage.enableMarkdown(true);
                    sendMessage.setChatId(player.getChatID());
                    sendMessage.setText("Choose your pray!");
                    try {
                        sendMessage = addInlineButtonsKill(sendMessage);
                        sendMessage(sendMessage);
                    } catch (TelegramApiException e) {
                        //System.out.println(e.toString());
                    }
                }
            }

        } else if(status == GameStatus.DAY) {
            broadcastMsg("IN GAME COMES TO DAY\n");
        }
    }








    public String getPlayerList() {
        StringBuffer sb = new StringBuffer();
        int count = 1;
        for (Player player: DB.getPlayerList()) {
            sb.append(count + ". " + player.getFname() + " " + player.getWins() + "\n");
            count++;
        }
        return sb.toString();
    }


    public void broadcastMsg(String message) {
        for (Player player: chats.values()) {
            sendMsg(player.getChatID(), message);
        }
    }
    public void broadcastMsgRole(String message, Role role) {
        for (Player player: chats.values()) {
            if(player.getRole() == role) sendMsg(player.getChatID(), message);
        }
    }
    public void broadcastMsgRole(String message, Role role, String exceptChatID) {
        for (Player player: chats.values()) {
            if(player.getRole() == role && !exceptChatID.equals(player.getChatID())) sendMsg(player.getChatID(), chats.get(exceptChatID).getFname() + ": " + message);
        }
    }

    public synchronized void sendMsg(String chatId, String s) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);
        try {
            addKeyboard(sendMessage);
            execute(sendMessage);
        } catch (TelegramApiException e) {
            System.out.println(e.toString());
        }
    }

    public String getBotUsername() {
        return BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }




    private SendMessage addInlineButtonsKill(SendMessage sendMessage) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        for (Player player: chats.values()) {
            List<InlineKeyboardButton> rowInline = new ArrayList<>();
            rowInline.add(new InlineKeyboardButton().setText(player.getFname()).setCallbackData("kill_player_" + player.getChatID()));
            rowsInline.add(rowInline);
        }
        markupInline.setKeyboard(rowsInline);
        sendMessage.setReplyMarkup(markupInline);
        return sendMessage;
    }
}
