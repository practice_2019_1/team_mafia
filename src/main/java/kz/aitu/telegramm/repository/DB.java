package kz.aitu.telegramm.repository;

import kz.aitu.telegramm.entity.Player;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DB {

    public static List<Player> getPlayerList() {
        List<Player> result = new ArrayList<>();

        String SQL_SELECT = "Select * from player";

        // auto close connection and preparedStatement
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/mafia", "postgres", "Techno2015");
             PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {

                Player player = new Player();
                player.setId(resultSet.getLong("id"));
                player.setFname(resultSet.getString("fname"));
                player.setWins(resultSet.getInt("wins"));
                player.setChatID(resultSet.getString("chatid"));

                result.add(player);

            }

        } catch (SQLException e) {
            System.err.format("SQL : %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return result;

    }

    public static void addPlayer(Player player) {
        String SQL_INSERT = "INSERT INTO player(chatid, fname, wins) "
                + "VALUES(?,?,0)";

        // auto close connection and preparedStatement
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/mafia", "postgres", "Techno2015");
             PreparedStatement psmt = conn.prepareStatement(SQL_INSERT)) {
            psmt.setString(1, player.getChatID());
            psmt.setString(2, player.getFname());

            psmt.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }


}
